<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis()
    {
        return view('form.register');
    }

    public function send(Request $request)
    {
        // dd($request->all());

        $fname = $request['NamaDepan'];
        $lname = $request['NamaBelakang'];
        return view('/welcome', ['fname' => $fname, 'lname' => $lname]);
    }
}
