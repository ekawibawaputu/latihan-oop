
<?php
require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');


$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>";
echo "legs : " . $sheep->legs;
echo "cold blooded :" . $sheep->cold_blooded . "<br>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs;
echo "cold blooded :" . $kodok->cold_blooded;
echo "Yell : ";
echo $kodok->jump();
echo "<br><br>";


$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold blooded :" . $sungokong->cold_blooded;
echo "Yell :";
echo $sungokong->yell();
?>